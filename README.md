# README #

For Linux:

How to install the script:

One of the possible ways to install it is to navigate to the dir containing the setup.py file 
and run the "pip3 install ." command.


After the script is installed it can be run in the following way:

zipfind path/to/input.csv path/to/output.csv --log=warning --verbose

This command will run the script in the verbose mode with logging on the warning level.