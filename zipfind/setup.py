from setuptools import setup

setup(
    name='zipfind',
    version='0.0.1',
    py_modules=['zipfind', 'scrape_nonsolocap', 'constants'],
    install_requires=[
        'pandas',
        'bs4',
        'requests',
        'lxml',
        'click',
    ],
    entry_points='''
    [console_scripts]
    zipfind=zipfind:main
    ''',
)
