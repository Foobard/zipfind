from bs4 import BeautifulSoup
from constants import CITIES
import numpy as np
import logging
import csv
import re


class NonsolocapCallback:
    def __init__(self, output):
        self.writer = csv.writer(open(output, 'w'), delimiter=';')
        self.fields = ('Address', 'City', 'Postal Code')
        self.writer.writerow(self.fields)

    def __call__(self, html, street_name, city_name, house_number):
        city = City(html, city_name, street_name)
        house = convert_to_int(house_number)
        if city_name.lower() in CITIES:
            if len(city.street) > 1:
                if house:
                    for street_area in city.street:
                        if house in street_area:
                            postal_code = street_area.get_postal_code()
                    if postal_code == '':
                        logging.warning("postal code not found: house '{}' is not in the street '{}'".
                                        format(house_number, street_name))
                else:
                    postal_code = ''
                    logging.warning('postal code cannot be determined: it depends on the house number which is missing')
            elif len(city.street) == 1:
                postal_code = city.street[0].get_postal_code()
            else:
                postal_code = ''
                logging.warning("postal code not found: street '{}' not found in the city '{}'".
                                format(street_name, city_name))
        else:
            postal_code = city.get_postal_code()
        self.writer.writerow(make_row(street_name, house_number, city_name, postal_code))


class City:
    def __init__(self, html, city_name, street_name):
        self.html = html
        self.soup = BeautifulSoup(self.html, 'lxml')
        self.city_name = city_name
        self.street_name = street_name
        self.city = self.locate(city_name, 'comune')
        self.street = [Street(location) for location in self.locate(street_name, 'indirizzo')]

    def locate(self, place_name, place_type):
        nw = self.soup.find(class_='nw')
        if nw:
            if any([place_type in th.text.lower() for th in nw.select('th')]):
                objects = self.soup.find_all(class_='rm')
                if len(objects) > 1:
                    objects = filter(lambda item: match(item, place_name), objects)
                if objects:
                    return [obj.find_parent() for obj in objects]
                else:
                    return ''
            else:
                return ''
        else:
            return ''

    def get_postal_code(self):
        if self.city:
            if len(self.city) > 1:
                logging.warning('found more than one match: {}'.format(self.city_name))
                return ''
            if len(self.city) == 1:
                return self.city[0].find(class_='vn').text
        else:
            logging.warning('no such city: {}'.format(self.city_name))
            return ''


class Street:
    def __init__(self, street):
        self.street = street

    def __contains__(self, house_number):
        house_numbers = self.get_house_numbers()
        sameparity = all([house_number % 2 == number % 2 for number in house_numbers])
        inrange = isinrange(house_numbers, house_number)
        if inrange and sameparity:
            return True
        else:
            return False

    def get_postal_code(self):
        if self.street:
            return self.street.find(class_='tg').text
        else:
            return ''

    def get_house_numbers(self):
        note = self.street.find(class_='tx')
        return [int(re.match('\d+', number.string).group()) for number in note.find_all('b')]


def isinrange(nrange, number):
    start, end = nrange
    return start <= number <= end


def make_row(street, house_number, city, postal_code):
    if isinstance(house_number, str):
        street = '{}, {}'.format(street, house_number)
    return (street, city, postal_code)


def convert_to_int(str_number):
    try:
        return int(re.match('\d+', str_number).group())
    except (AttributeError, TypeError):
        return ''


def match(item, place_name):
    item_text_lower = item.string.lower()
    place_name_lower = place_name.lower()
    item_words = np.array(item_text_lower.split())
    place_name_words = np.array(filter(lambda word: '.' not in word, place_name_lower.split()))
    if item_text_lower == place_name_lower:
        return True
    elif np.in1d(item_words, place_name_words).all() or np.in1d(place_name_words, item_words).all():
        logging.warning('{} not found. Perhaps you meant {}'.format(place_name_lower, item_text_lower))
        return False
    else:
        return False
