from constants import CITIES, DELAY, URL, LOGGING_LEVELS
from scrape_nonsolocap import NonsolocapCallback
import pandas as pd
import requests
import logging
import click
import time

try:
    from urllib import urlencode
except ImportError:
    from urllib.parse import urlencode


def download(url, console_output, delay=DELAY):
    if console_output:
        print('Downloading: {}'.format(url))
    logging.info('Downloading: {}'.format(url))
    time.sleep(delay)
    response = requests.get(url)
    return response.text


def link_crawler(path_to_data, console_output, separator=';', scrape_callback=None):
    raw_data = pd.read_csv(path_to_data, sep=separator)
    formatted_data = format_data(raw_data)

    links = get_links(formatted_data.street_place)
    streets = formatted_data.Address.tolist()
    cities = formatted_data.City.tolist()
    house_numbers = formatted_data.house_number.tolist()

    while links:
        html = download(links.pop(0), console_output=console_output)
        if scrape_callback:
            links.extend(scrape_callback(html,
                                         streets.pop(0),
                                         cities.pop(0),
                                         house_numbers.pop(0)) or [])


def format_data(data):
    data['house_number'] = data.Address.str.extract('(\d[/\w]*)')
    data.Address.replace(['[\d,] ?[\w\/,]+ ?', 'V. '], ['', 'Via '], regex=True, inplace=True)
    data = data.apply(lambda series: series.apply(lambda obj: obj.strip() if isinstance(obj, str) else obj))
    data['street_place'] = data[['Address', 'City']].apply(tuple, axis=1)
    return data


def make_query_string(street_place):
    if street_place[1].lower() in CITIES:
        return urlencode(list(zip(('k', 'c'), street_place)))
    else:
        return urlencode(list(zip(('k', ), street_place[-1:])))


def get_links(street_place):
    queries = street_place.apply(make_query_string)
    return queries.apply(lambda query: '{}/cap?{}'.format(URL, query)).tolist()


@click.command(help='find zip codes given a column of streets and towns')
@click.argument('INPUT.csv', type=click.Path(exists=True, file_okay=True), metavar='INPUT.csv')
@click.argument('OUTPUT.csv', type=click.Path(exists=False, file_okay=True), metavar='OUTPUT.csv')
@click.option('--verbose', '-v', is_flag=True, default=False, help='log download progress to console')
@click.option('--log', '-l', nargs=1, type=click.Choice(LOGGING_LEVELS), metavar='LOG_LEVEL', required=False, help='enable logging')
def main(**arguments):
    if arguments['log']:
        logging.basicConfig(filename='zipfind.log', filemode='w', level=getattr(logging, arguments['log'].upper()))
        logging.info('Started <<<<<<<<<<<<<<<<<<')
        link_crawler(arguments['input.csv'], console_output=arguments['verbose'], scrape_callback=NonsolocapCallback(arguments['output.csv']))
        logging.info('Finished >>>>>>>>>>>>>>>>>')
    else:
        link_crawler(arguments['input.csv'], console_output=arguments['verbose'], scrape_callback=NonsolocapCallback(arguments['output.csv']))
