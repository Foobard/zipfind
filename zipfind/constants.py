CITIES = ['alesandria', 'ancona', 'bari', 'bergamo', 'bologna',
          'brescia', 'cagliari', 'catania', 'cesena', 'ferrara',
          'firenze', 'foggia', 'forli', 'genova', 'la spezia',
          'livorno', 'messina', 'milano', 'modena', 'napoli',
          'nuova pescara', 'padova', 'palermo', 'parma',
          'perugia', 'pesaro', 'pescara', 'piacenza', 'pisa',
          'ravenna', 'reggio calabria', 'reggio emilia',
          'rimini', 'roma', 'salerno', 'taranto', 'torino',
          'trento', 'trieste', 'venezia', 'verbania', 'verona']

DELAY = 5

URL = 'http://www.nonsolocap.it'

LOGGING_LEVELS = ['debug', 'info', 'warning', 'critical']
